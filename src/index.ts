import { run } from '@cycle/run';
import xs from 'xstream';
import { DOMSource, makeDOMDriver, VNode, h1 } from '@cycle/dom';

export interface Sources {
  dom: DOMSource;
}

const main = (sources: Sources) => {
  return {
    dom: xs
      .create()
      .startWith(0)
      .map(i => h1('cool ' + i))
  };
};

const drivers = {
  dom: makeDOMDriver('#app')
};

run(main, drivers);
